import os
import pygame
import glob
import random

images = []
logos = []
bgi = pygame.Surface((1280, 720))
xr = 320
yr = 240
logo_x = 50
logo_y = 50
counter = 0

def draw_random_image(screen):
    global images, image, bgi, xr, yr
    
    image = random.choice(images)
    (img_width, img_height) = image.get_size()
    top = abs((img_height - yr)/2)
    print 'top: ' + str(top)
    print 'xr: ' + str(xr)
    print 'yr: ' + str(yr)
    bgi.blit(image, (0, 0), (0, top, xr, yr))

def setup(screen, etc) :
    global images, logos, xr, yr, image, logo_x, logo_y, logo, counter

    counter = 0
    
    for filepath in sorted(glob.glob(etc.mode_root + '/Images/*.jpg')): 
        filename = os.path.basename(filepath)
        print 'loading image file: ' + filename
        img = pygame.image.load(filepath)
        images.append(img)
    
    for filepath in sorted(glob.glob(etc.mode_root + '/Logos/*.png')): 
        filename = os.path.basename(filepath)
        print 'loading logo file: ' + filename
        logo = pygame.image.load(filepath)
        logo = logo.convert_alpha()
        logos.append(logo)

    xr = etc.xres
    yr = etc.yres
    logo_x = (xr - 400)/2
    logo_y = (yr - 400)/2
    
    draw_random_image(screen)

def draw(screen, etc) :
    global counter, logo
    if etc.audio_trig:
        draw_random_image(screen)
    
    counter = counter + 1
    
    if counter == 2:
        logo = random.choice(logos)
        logo = pygame.transform.scale(logo, (400, 400))
        counter = 0

    screen.blit(bgi, (0, 0))
    screen.blit(logo, (logo_x, logo_y))
