import os
import pygame
import random

words = ["CROZ", "ALEX", "JOE", "JOEY", "THE PYLONS", "GIMPS", "WHOBBLE", "MARVIN", "11AM NEGRONI CLUB"]
colours = ["RED", "GREEN", "BLUE"]
xr = 320
yr = 240

def setup(screen, etc):
    global bg, text, colours, font, words, font, word

    bg = random.choice(colours)
    text = random.choice(colours)
    word = random.choice(words)

    font = pygame.freetype.Font(etc.mode_root + "/Oswald-Bold.ttf", 200)
    xr = etc.xres
    yr = etc.yres

def draw(screen, etc):
    global text, bg, colours, font, word, words

    if etc.audio_trig:
        bg = random.choice(colours)
        text = random.choice(colours)
        word = random.choice(words)

    bg_colour = (0,0,0)
    diff = 20
    if bg == "RED":
        bg_colour = (255 - random.randrange(diff),random.randrange(diff), random.randrange(diff))
    if bg == "GREEN":
        bg_colour = (random.randrange(diff), 255 - random.randrange(diff), random.randrange(diff))
    if bg == "BLUE":
        bg_colour = (random.randrange(diff), random.randrange(diff), 255 - random.randrange(diff))

    text_colour = (0,0,0)
    if text == "RED":
        text_colour = (255 - random.randrange(diff), random.randrange(diff), random.randrange(diff))
    if text == "GREEN":
        text_colour = (random.randrange(diff), 255 - random.randrange(diff), random.randrange(diff))
    if text == "BLUE":
        text_colour = (random.randrange(diff), random.randrange(diff), 255 - random.randrange(diff))

    (render, _) = font.render(word, fgcolor=text_colour)
    render = pygame.transform.scale(render, (int(1280 * 0.9), int(720 * 0.9)))
    background = pygame.Surface((1280, 720))
    background.fill(bg_colour)

    screen.blit(background, (0, 0))
    screen.blit(render, (int(0.05 * 1280), int(0.05 * 720)))
